# otacon_statusbar

![screenshot](screenshot.png)

## Description

A minimal statusbar optimized for the narrow lengths seen in mobile terminal use. It was based on a rice that I've seen on [unixporn](https://reddit.com/r/unixporn), though I can't seem to find it again.

## Installation

```
curl -s https://gitlab.com/hThoreau/otacon_statusbar/raw/master/tmux.conf >> ~/.tmux.conf
```